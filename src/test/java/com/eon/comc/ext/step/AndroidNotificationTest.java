package com.eon.comc.ext.step;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eon.comc.ext.dto.AddressDTO;
import com.eon.comc.ext.dto.CommDataDTO;
import com.eon.comc.ext.dto.types.IContentType;
import com.eon.comc.ext.dto.types.ParamArea;
import com.eon.comc.ext.exception.ProcessException;
import com.eon.comc.ext.process.ProcessContext;
import com.eon.comc.ext.process.ProcessMessage;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AndroidNotification.class, LoggerFactory.class })
public class AndroidNotificationTest {
	
	// variables for constructSms
	String smsText = null;
	IContentType content = null;

	// process
	Map<ParamArea, Map<String, String>> params = null;
	ProcessContext context = null;
	ProcessMessage message = null;

	@Before
	public void setUp() throws Exception {
		// mock logger
		mockStatic(LoggerFactory.class);
		Logger logger = mock(Logger.class);
		when(LoggerFactory.getLogger(any(Class.class))).thenReturn(logger);

		// prepare data for testProcess
		// context
		Map<String, String> mapLocal = new HashMap<String, String>();
		
		mapLocal.put("msgDataTitle", "Test message");
		mapLocal.put("msgDataText", "Content of test message");
		mapLocal.put("msgDataScenario", "dummyScenarioId");
		
		mapLocal.put("apiKey", "AIzaSyDYwP_DeeAKtKxtqxWUcdblSXzsBU4eODM");
		mapLocal.put("apiUrl", "https://android.googleapis.com/gcm/send");
		mapLocal.put("msgCollapseKey", "scenario");	// group where last msg is valid
		mapLocal.put("msgTimeToLive", "600");	// 10 min
		mapLocal.put("msgDryRun", "true");		// testing flag
		
		params = new HashMap<ParamArea, Map<String, String>>();
		params.put(ParamArea.Local, mapLocal);
		context = new ProcessContext(params);
		
		List<AddressDTO> addresses = new ArrayList<AddressDTO>();
		AddressDTO address = new AddressDTO();
		address.setValue("APA91bH40UYOPtmGZFf-h3UMGtTXPTy2Z321Wnq59-TWn4qLh6hiJ_yk2meoUsbTeCWTzNZyh4JDeGcouPVKvMH6PvVrSiBAQXhrF0kNmRTLjcXcbfh0wtLzoYuEQYH4KcZYqz8VZ1t8");
		addresses.add(address);
		context.setAddresses(addresses);
	
		// message
		message = new ProcessMessage(0L, 0, 0, new DateTime());
		CommDataDTO commData = new CommDataDTO();
		commData.setiContent(content);
		message.setCommData(commData);
	}
	
	
	@Test
	/**
	 * jak vlatsne otestovat praci s maily ve schrance?
	 * staci, ze to nevyhodilo zadnou vyjimku
	 * a vratilo aktualni datum a cas na konci reportu (?)
	 */
	public void testProcess() {
		try {
			AndroidNotification ic = new AndroidNotification();				
			ic.process(context, message);
		} catch (ProcessException e) {
			fail("Exception raised " + e);
		}
		assertNotSame("Result report expected",	"",  message.getCommData().getPlainText());
	}
	
}
