package com.eon.comc.ext.step;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eon.comc.ext.dto.AddressDTO;
import com.eon.comc.ext.dto.types.IContentType;
import com.eon.comc.ext.dto.types.ItemType;
import com.eon.comc.ext.dto.types.ParamArea;
import com.eon.comc.ext.exception.ProcessException;
import com.eon.comc.ext.process.ProcessContext;
import com.eon.comc.ext.process.ProcessMessage;
import com.eon.comc.ext.process.QueueContent;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class AndroidNotification implements QueueContent {

	// Logging
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AndroidNotification.class);

	/**
	 * Parameters required:
	 * 		msgDataTitle
	 * 		msgDataText
	 * 		msgDataScenario
	 * 		TODO:	konzultovat jestli nemuzu predavat data zpravy jako
	 *				msgData - Set<String, String> s klici retezcu napr. title, message, scenario
	 *				abych mohl pouzivat libovolne klice pro prenos dat
	 *				nyni je to napevno nakodovane pouze na Title, Text, Scenario
	 *				To same pro messageSettings, ktere jsou nyni take napevno nakodovane (nikoliv vsechny)
	 *		apiKey	application apikey from gcm administation
	 *		apiUrl	url of gcm notification service
	 *		msgCollapseKey	subgroup of messages, only last message will be displayed
	 *		msgTimeToLive	message lifetime in gcm when not delivered to user
	 */
	@Override
	public void process(ProcessContext context, ProcessMessage message)
			throws ProcessException {

		LOGGER.debug("Android notification sender started");
		Map<String, String> localMap = context.getParams().get(ParamArea.Local);
		String report = "";

		if (!localMap.containsKey("msgDataTitle"))
			throw new ProcessException("msgDataTitle parameter not defined");
		if (!localMap.containsKey("msgDataText"))
			throw new ProcessException("msgDataText parameter not defined");
		if (!localMap.containsKey("msgDataScenario"))
			throw new ProcessException("msgDataScenario parameter not defined");
		
		if (!localMap.containsKey("apiKey"))
			throw new ProcessException("apiKey parameter not defined");
		if (!localMap.containsKey("apiUrl"))
			throw new ProcessException("apiUrl parameter not defined");
		if (!localMap.containsKey("msgCollapseKey"))
			throw new ProcessException("msgCollapseKey parameter not defined");
		if (!localMap.containsKey("msgTimeToLive"))
			throw new ProcessException("msgTimeToLive parameter not defined");
		
		// prepare list of registration IDs of devices
		List<String> androidTargets = new ArrayList<String>();
		for (AddressDTO address : context.getAddresses()) {
			androidTargets.add(address.getValue());
		}

		// init sender for messages with API key
		Sender sender = new Sender(localMap.get("apiKey"));
		
		// create message to send
		Message androidMessage = new Message.Builder()
			.collapseKey(localMap.get("msgCollapseKey"))
		    .timeToLive(Integer.parseInt(localMap.get("msgTimeToLive")))
		    .delayWhileIdle(true)
		    .addData("title", localMap.get("msgDataTitle"))
		    .addData("text", localMap.get("msgDataText"))
		    .addData("scenario", localMap.get("msgDataScenario"))
		    .build();
		
		try {
            MulticastResult result = sender.send(androidMessage, androidTargets, 1);
             
            if (result.getResults() != null) {
                Integer canonicalRegId = result.getCanonicalIds();
                if (canonicalRegId != 0) {
                	report = "Notificatin sent: " + canonicalRegId.toString() + "\n";
                	report = report + result.toString();
                }
            } else {
                Integer error = result.getFailure();
                throw new ProcessException("GCM failure code: " + error.toString());
            }
        } catch (Exception e) {
            throw new ProcessException("Notification sending error: " + e.getMessage());
        }
		
		message.getCommData().setPlainText(report);
	}

	@Override
	public void process(ProcessContext context, ProcessMessage message,
			List<AddressDTO> addresses) throws ProcessException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProcessMessage generate(ProcessContext context, Long id) throws ProcessException {
		// TODO Auto-generated method stub
		return null;
	}

}
